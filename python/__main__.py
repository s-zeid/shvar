#!/usr/bin/env python3

"""Parses a template according to shell variable syntax.

The following types of substitutions are supported:

* `$x` (simple variable substitution)
* `${x}` (simple substitution in braces)
* `${x-default}` (use "default" if x is unset)
* `${x=default}` (if x is unset, assign "default" to x and use "default")
* `${x?error}` (if x is unset, abort with "error")
* `${x+alternate}` (if x is not unset, use "alternate"; otherwise, use "")
* `${x:-default}` (use "default" if x is empty (unset or the empty string))
* `${x:=default}` (if x is empty, assign "default" to x and use "default")
* `${x:?error}` (if x is empty, abort with "error")
* `${x:+alternate}` (if x is not empty, use "alternate"; otherwise, use "")
* `${x#prefix}` (remove the smallest matching prefix from the end of x)
* `${x##prefix}` (remove the largest matching prefix from the end of x)
* `${x%suffix}` (remove the smallest matching suffix from the end of x)
* `${x%%suffix}` (remove the largest matching suffix from the end of x)
* `${#x}` (use the length of x)

For substitutions with modifiers, the right hand side of the modifier
will be expanded according to the same rules.

A backslash (`\\`) can be used to escape a dollar sign (`$`), another
backslash, a closing brace (`}`) on the right hand side of a modifier,
a double quote (`"`) in all contexts, and a single quote (`'`) in
prefix/suffix patterns.

Double quotes are removed in all contexts unless escaped.  Single quoted
strings are not treated specially except in prefix/suffix patterns,
where the single quotes are removed and the literal string is substitued
without further expansion.

Prefixes and suffixes may be shell glob patterns, except that character
classes are not supported.

No other expansions, such as pathname expansion or command substitution,
are performed.

"""


import argparse
import os
import sys

from . import errors
from . import tests

from . import substitute


def main(argv):
 p = argparse.ArgumentParser(
  description=__doc__.split("\n\n", 1)[0],
  usage="shvar [-h] [-v NAME=VALUE] [-e] [-p PROGRAM] TEMPLATE"
 )
 p.add_argument("--hep", dest="_hep_easter_egg", action="store_true",
                help=argparse.SUPPRESS)
 p.add_argument("--docs", action="store_true",
                help="show extended help and exit")
 p.add_argument("-v", "--var", action="append", dest="scope_specs", metavar="NAME=VALUE",
                help="define a variable")
 p.add_argument("-e", "--env", action="store_true",
                help="allow access to environment variables"
                     " (always true if -v/--var is absent)")
 p.add_argument("-u", "--no-unset", action="store_true",
                help="fail if a variable is not set, and a [:]-=?+ modifier"
                     " was not used")
 p.add_argument("-p", "--prog", default=None, metavar="PROGRAM",
                help="the program name to use in null or unset error messages")
 p.add_argument("-s", "--sigil", default=None,
                help="the sigil to use for variable names (default: `$`)")
 p.add_argument("-b", "--braces", default=None,
                help="the set of braces to use (default: `{}`)")
 p.add_argument("template", nargs = argparse.REMAINDER, metavar="TEMPLATE",
                help="the template to evaluate")
 
 g = p.add_argument_group("developer arguments")
 g.add_argument("--test", action="count",
                help="run tests and exit")
 g.add_argument("--test-exec", default=None, metavar="PROG",
                help="test another program that behaves like this one")
 g.add_argument("--from-test", action="store_true", help=argparse.SUPPRESS)
 g.add_argument("--print-tests", action="store_true", help=argparse.SUPPRESS)
 g.add_argument("--test-newline-before-fail", action="store_true", help=argparse.SUPPRESS)
 
 try:
  options = p.parse_args(argv[1:])
 except SystemExit as exc:
  return exc.code
 
 if options._hep_easter_egg:
  print("Hep!  Hep!  I'm covered in sawlder! ... Eh?  Nobody comes.")
  print("--Red Green, https://www.youtube.com/watch?v=qVeQWtVzkAQ#t=6m27s")
  return 0
 
 if options.docs:
  docs = p.format_help() + "\n\n" + __doc__.split("\n\n", 1)[1].rstrip()
  pager = os.environ.get("PAGER", "")
  if pager and sys.stdin.isatty() and sys.stdout.isatty() and sys.stderr.isatty():
   try:
    import subprocess
    r = subprocess.run([pager], input=docs.encode("utf-8") + b"\n").returncode
    if r:
     msg = f"pager `{pager}` failed with return code {r} (try piping into cat)"
     print(f"{p.prog}: error: {msg}", file=sys.stderr)
    return r
   except FileNotFoundError:
    pass
  print(docs)
  return 0
 
 if options.print_tests:
  import json
  test_list = tests.get()
  for test in test_list:
   test[3] = test[3].__name__
  tests_repr = "[\n%s\n]" % ",\n".join([" " + json.dumps(test) for test in test_list])
  print(tests_repr)
  return 0
 
 if options.test or options.test_exec:
  return 0 if tests.run(
   options.test or 0,
   options.test_exec,
   options.test_newline_before_fail
  ) else 1
 
 template_args = options.template
 if len(template_args) and template_args[0] == "--":
  template_args = template_args[1:]
 
 if not len(template_args):
  try:
   p.error("a template is required")
  except SystemExit as exc:
   return exc.code
 
 scope = os.environ.copy() if options.env or not options.scope_specs else {}
 
 if options.scope_specs:
  for spec in options.scope_specs:
   if not len(spec) or "=" not in spec or spec[0] == "=":
    try:
     p.error("-v/-var must be of the format NAME=VALUE")
    except SystemExit as exc:
     return exc.code
   k, v = spec.split("=", 1)
   scope[k] = v
 
 try:
  print(substitute(
   " ".join(template_args), scope,
   no_unset=options.no_unset,
   sigil=options.sigil, braces=options.braces
  ))
 except errors.KeyError as exc:
  prog = options.prog if options.prog is not None else p.prog
  ret = 5 if exc.message else 4
  msg = exc.message
  if not options.from_test:
   sep = ": " if prog else ""
   if exc.maybe_null:
    msg = msg or f"{exc.var_name}: parameter null or not set"
   else:
    msg = msg or f"{exc.var_name}: parameter not set"
   msg = f"{prog}{sep}{msg}"
  if msg:
   print(msg, file=sys.stderr)
  return ret
 except errors.SyntaxError as exc:
  msg = str(exc)
  if not options.from_test:
   msg = f"{p.prog}: syntax error: {exc}"
  if msg:
   print(msg, file=sys.stderr)
  return 3
 except errors.SHVarError as exc:
  if not options.from_test and ("sigil" in str(exc) or "brace" in str(exc)):
   print(f"{p.prog}: error: {exc}", file=sys.stderr)
   return 1
  else:
   raise


def _main():
 try:
  sys.exit(main(sys.argv))
 except KeyboardInterrupt:
  pass


if __name__ == "__main__":
 _main()
