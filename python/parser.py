from . import errors
from . import pattern


class Parser:
 def __init__(self, template, scope):
  self.tmpl = template.tmpl
  self.no_unset = template.no_unset
  self.sigil = template.sigil if template.sigil is not None else '$'
  self.braces = template.braces if template.braces is not None else '{}'
  self.brackets = "[]"
  self.scope = scope
  
  if self.scope is None:
   self.scope = {}
  
  if (isinstance(self.sigil, str) and isinstance(self.braces, str) and
      len(self.sigil) and self.sigil in self.braces):
   raise errors.OtherError(f"sigil `{self.sigil}` and braces `{self.braces}` conflict")
  
  if (not isinstance(self.sigil, str) or len(self.sigil) != 1 or
      self._is_important(self.sigil, is_sigil=True)):
   raise errors.OtherError(f"`{self.sigil}` is not a valid sigil")
  
  if (not isinstance(self.braces, str) or len(self.braces) != 2 or
      self.braces[0] == self.braces[1]):
   raise errors.OtherError(f"`{self.braces}` is not a valid set of braces")
  
  for i in self.braces:
   if self._is_important(i, is_brace=True):
    raise errors.OtherError(f"`{i}` is not a valid brace")
  
  if self.braces[0] == self.brackets[0]:
   self.brackets = "{}"
  
  self.pos = 0
  self.brace_level = 0
 
 def parse(self):
  return self._parse_str(0, True, "")
  
 def _parse_str(self, start_brace_level, ignore_single_quotes, escape_quoted_chars):
  tmpl = self.tmpl
  
  result = ""
  
  in_double_quote = False
  in_single_quote = False
  
  escape_backslashes = '\\' in escape_quoted_chars
  
  while self.pos < len(tmpl):
   ch = tmpl[self.pos]
   
   if not in_single_quote:
    if ch == '\\':
     if self.pos + 1 < len(tmpl):
      next_ch = tmpl[self.pos + 1];
      if tmpl[self.pos + 1] in ('\\', '\'', '"', self.sigil):
       if next_ch == '\\' and escape_backslashes:
        result += '\\'
       result += next_ch
       self.pos += 2
       continue
      elif self.brace_level > start_brace_level and next_ch == self.braces[1]:
       result += next_ch
       self.pos += 2
       continue
    
    if ch == self.sigil:
     self.pos += 1
     parse_result = self._parse_var()
     result += parse_result
     if self.pos >= len(tmpl):
      break
     continue
    
    if not in_double_quote:
     if self.brace_level > start_brace_level and ch == self.braces[1]:
      self.brace_level -= 1
      self.pos += 1
      break
   
   if ch == '"':
    in_double_quote = not in_double_quote
   elif ch == "'" and not ignore_single_quotes and not in_double_quote:
    in_single_quote = not in_single_quote
   else:
    if in_single_quote or in_double_quote:
     if ch in escape_quoted_chars:
      result += '\\'
    result += ch
   
   self.pos += 1
   continue
  
  return result
 
 def _parse_var(self):
  tmpl = self.tmpl
  
  start_brace_level = self.brace_level
  first_ch = True
  var_name = ""
  default = None
  want_length = False
  brace_op = ""
  brace_op_double = False
  brace_op_rhs = None
  is_colon_op = False
  result = ""
  
  while self.pos < len(tmpl):
   ch = tmpl[self.pos]
   
   if self.brace_level > start_brace_level:
    if ch == self.braces[1]:
     self.brace_level -= 1
     self.pos += 1
     break
   
   if first_ch:
    if ch == self.braces[0]:
     self.brace_level += 1
     self.pos += 1
     continue
    
    if self._is_ident_first(ch):
     var_name += ch
     first_ch = False
    elif self._is_number(ch):
     var_name += ch
     if self.brace_level <= start_brace_level:
      self.pos += 1
      break
    elif self.brace_level > start_brace_level:
     if ch == "#":
      want_length = 1
      self.pos += 1
      continue
     elif self._is_brace_special(ch):
      raise errors.SyntaxError("no identifier found in brace expansion")
     else:
      raise errors.SyntaxError("invalid character `%s` in brace expansion" % ch)
    else:
     result += self.sigil + ch
     self.pos += 1
     break
   
   else:  # not first_ch
    if self._is_ident(ch):
     var_name += ch
    elif self.brace_level > start_brace_level:
     if ch == ":":
      if self.pos + 1 < len(tmpl):
       is_colon_op = True
       self.pos += 1
       ch = tmpl[self.pos]
      else:
       break
     if self._is_modifier(ch):
      brace_op = ch
      self.pos += 1
      brace_op_rhs = self._parse_str(start_brace_level, True, "")
      break
     elif is_colon_op:
      if ch == self.braces[1]:
       ch = ""
      raise errors.SyntaxError("invalid modifier `:%s` in brace expansion" % ch)
     elif ch == '#' or ch == '%':
      brace_op = ch
      self.pos += 1
      if self.pos < len(tmpl) and tmpl[self.pos] == brace_op:
       brace_op_double = True
       self.pos += 1
      brace_op_rhs = self._parse_str(start_brace_level, False, r"\?*[")
      break
     else:
      raise errors.SyntaxError("invalid modifier `%s` in brace expansion" % ch)
    else:
     break
   
   self.pos += 1
   continue
  
  if self.brace_level > start_brace_level:
   raise errors.SyntaxError("unterminated brace expansion")
  elif var_name:
   if var_name in self.scope:
    value = str(self.scope[var_name])
    val_is_set = True
   else:
    value = ""
    val_is_set = False
   
   if is_colon_op:
    use_value = bool(value)
   else:
    use_value = val_is_set
   
   if not self._is_modifier(brace_op) and self.no_unset and not val_is_set:
    raise errors.KeyError(None, var_name, False)
   
   if want_length:
    if brace_op:
     raise errors.SyntaxError("cannot use length operator and modifier at the same time")
    result = str(len(value))
   elif brace_op:
    if brace_op == "-":
     result = value if use_value else brace_op_rhs
    if brace_op == "=":
     if not use_value:
      self.scope[var_name] = result = brace_op_rhs
     else:
      result = value
    if brace_op == "?":
     if use_value:
      result = value
     else:
      raise errors.KeyError(brace_op_rhs, var_name, is_colon_op)
    if brace_op == "+":
     result = brace_op_rhs if use_value else ""
    if brace_op == "#":
     result = value
     if brace_op_rhs:
      pat = pattern.Pattern(brace_op_rhs, self.brackets)
      result = pat.remove_from_start(result, brace_op_double)
    if brace_op == "%":
     result = value
     if brace_op_rhs:
      pat = pattern.Pattern(brace_op_rhs, self.brackets)
      result = pat.remove_from_end(result, brace_op_double)
   else:
    result = value
  
  return result
 
 def _is_letter(self, ch):
  return 0x41 <= ord(ch) <= 0x5a or 0x61 <= ord(ch) <= 0x7a
 def _is_number(self, ch):
  return 0x30 <= ord(ch) <= 0x39
 def _is_ident_first(self, ch):
  return self._is_letter(ch) or ch == '_'
 def _is_ident(self, ch):
  return self._is_ident_first(ch) or self._is_number(ch)
 def _is_modifier(self, ch):
  return ch and ch in '-=?+'
 def _is_brace_special(self, ch):
  return self._is_modifier(ch) or ch in ':#%'
 def _is_important(self, ch, is_brace=False, is_sigil=False):
  if self._is_ident(ch) or ch == '\\':
   return True
  if self._is_brace_special(ch) and not is_sigil:
   return True
  if ch == self.sigil and not is_sigil:
   return True
  if ch in self.braces and not is_brace:
   return True
  return False
