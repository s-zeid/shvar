from typing import *  #type: ignore

from . import errors


class _PatternItem:
 def __repr__(self):
  return type(self).__name__

class _PatternOneItem(_PatternItem):
 pass

class _PatternAnyItem(_PatternItem):
 pass

class _PatternRangesItem(_PatternItem, NamedTuple):  #type: ignore
 ranges: "_PatternRanges"

class _PatternStringItem(_PatternItem, NamedTuple):  #type: ignore
 chars: str


class _PatternRange(NamedTuple):
 start: str
 end: str
 
 def matches(self, ch: str):
  if len(ch) != 1:
   raise errors.OtherError("argument must be a single character")
  return ord(self.start) <= ord(ch) <= ord(self.end)


class _PatternRanges(NamedTuple):
 ranges: List[_PatternRange]
 inverse: bool
 
 def match(self, ch: str):
  test_ch_matches = any(map(lambda range: range.matches(ch), self.ranges))
  if self.inverse:
   test_ch_matches = not test_ch_matches
  return test_ch_matches


class Pattern:
 chars: str
 items: List[_PatternItem]
 brackets: str
 is_literal: bool
 
 def __init__(self, pattern: str, brackets: str = '[]'):
  self.chars = pattern
  self.items = []
  self.brackets = brackets
  self.is_literal = True
  
  if (not isinstance(self.brackets, str) or len(self.brackets) != 2 or
      self.brackets[0] == self.brackets[1]):
   raise errors.OtherError(f"`{self.brackets}` is not a valid set of brackets")
  
  for i in self.brackets:
   if i == '?' or i == '*':
    raise errors.OtherError(f"`{i}` is not a valid brace")
  
  self._compile()
 
 def _compile(self):
  if len(self.items) > 0:
   raise RuntimeError("already compiled")
  
  p_pos = 0
  
  while p_pos < len(self.chars):
   prev_item = self.items[-1] if len(self.items) > 0 else None
   ch = self.chars[p_pos]
   p_pos += 1
   
   if ch == '\\':
    ch = self.chars[p_pos]
    p_pos += 1
   
   elif ch == '?':
    self.is_literal = False
    self.items += [_PatternOneItem()]
    continue
   
   elif ch == '*':
    self.is_literal = False
    if len(self.items) == 0 or not isinstance(prev_item, _PatternAnyItem):
     self.items += [_PatternAnyItem()]
    continue
   
   elif ch == self.brackets[0]:
    self.is_literal = False
    inverse = False
    ranges: List[_PatternRange] = []
    bracket_pos = 0
    first_range = True
    while True:
     if p_pos >= len(self.chars):
      p_pos -= bracket_pos
      bracket_pos = 0
      ch = self.brackets[0]
      break
     ch = self.chars[p_pos]
     if not first_range and ch == self.brackets[1]:
      p_pos += 1
      break
     if bracket_pos == 0 and ch == '!':
      inverse = True
      bracket_pos += 1
     elif (p_pos + 2 < len(self.chars) and self.chars[p_pos + 1] == '-' and
           self.chars[p_pos + 2] != self.brackets[1]):
      start, end = ch, self.chars[p_pos + 2]
      if ord(start) > ord(end):
       raise errors.OtherError("invalid character range `%s`" % self.chars[p_pos:p_pos+2])
      ranges += [_PatternRange(start, end)]
      first_range = False
      bracket_pos += 3
      p_pos += 2
     else:
      ranges += [_PatternRange(ch, ch)]
      first_range = False
      bracket_pos += 1
     p_pos += 1
    if bracket_pos > 0:
     self.items += [cast(_PatternItem, _PatternRangesItem(_PatternRanges(ranges, inverse)))]
     continue
   
   prev_item = self.items[-1] if len(self.items) > 0 else None
   if len(self.items) >= 1 and isinstance(prev_item, _PatternStringItem):
    self.items[-1] = cast(_PatternItem, _PatternStringItem(prev_item.chars + ch))
   else:
    self.items += [cast(_PatternItem, _PatternStringItem(ch))]
 
 def matches(self, test: str, _from: int = 0) -> bool:
  result = True
  
  p_pos = _from
  t_pos = 0
  
  while p_pos < len(self.items):
   item = self.items[p_pos]
   
   if isinstance(item, _PatternOneItem):
    if t_pos < len(test):
     t_pos += 1
    else:
     result = False
   
   elif isinstance(item, _PatternAnyItem):
    next_item = self.items[p_pos + 1] if p_pos + 1 < len(self.items) else None
    while t_pos < len(test):
     if isinstance(next_item, _PatternStringItem) and test[t_pos] != next_item.chars[0]:
      t_pos += 1
     elif next_item is None:
      t_pos += 1
     else:
      break
    if next_item is not None:
     while t_pos < len(test):
      if self.matches(test[t_pos:], _from=p_pos + 1):
       return True
      t_pos += 1
     result = False
   
   elif isinstance(item, _PatternRangesItem):
    if t_pos < len(test) and item.ranges.match(test[t_pos]):
     t_pos += 1
    else:
     result = False
   
   elif isinstance(item, _PatternStringItem):
    if t_pos < len(test) and test[t_pos:].startswith(item.chars):
     t_pos += len(item.chars)
    else:
     result = False
   
   else:
    raise NotImplementedError("unimplemented pattern item type `%s`" % type(item).__name__)
   
   if not result:
    break
   
   p_pos += 1
  
  if p_pos < len(self.items) or t_pos < len(test):
   result = False
  
  return result
 
 def remove_from_start(self, value: str, largest: bool) -> str:
  prefix_end_range: Iterable
  prefix_end_range = irange(1, len(value))
  if largest:
   prefix_end_range = reversed(prefix_end_range)
  
  for prefix_end in prefix_end_range:
   prefix_candidate = value[0:prefix_end]
   if self.matches(prefix_candidate):
    return value[prefix_end:]
  
  return value
 
 def remove_from_end(self, value: str, largest: bool) -> str:
  suffix_start_range: Iterable
  suffix_start_range = irange(0, len(value) - 1)
  if not largest:
   suffix_start_range = reversed(suffix_start_range)
  
  for suffix_start in suffix_start_range:
   suffix_candidate = value[suffix_start:]
   if self.matches(suffix_candidate):
    return value[0:suffix_start]
  
  return value


def irange(start: int, stop: int, step: int = 1) -> range:
 """Return a range that is inclusive of its stop value."""
 return range(start, stop + (1 if step > 0 else -1), step)
