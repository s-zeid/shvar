#!/usr/bin/env python3

# This file is public domain via CC0:
# <https://creativecommons.org/publicdomain/zero/1.0/>

import sys

from setuptools import setup, find_packages


setup(
 name="shvar",
 version="0.0.1",
 description="",
 url="https://code.s.zeid.me/shvar",
 author="S. Zeid",
 author_email="support+shvar-python@s.zeid.me",
 license="X11 License:  https://tldrlegal.com/license/x11-license",
 classifiers=[
  "Development Status :: 3 - Alpha",
  "Environment :: Console",
  "Intended Audience :: Developers",
  "Natural Language :: English",
  "Programming Language :: Python :: 3",
  "Topic :: Software Development :: Libraries :: Python Modules",
  "Topic :: Utilities",
 ],
 package_dir={
  "shvar": "."
 },
 packages=[
  "shvar"
 ],
 entry_points={
  "console_scripts": [
   "shvar=shvar.__main__:main",
  ]
 },
)
