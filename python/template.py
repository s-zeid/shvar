from . import parser


class Template:
 def __init__(self, tmpl, no_unset=False, sigil=None, braces=None):
  self.tmpl = tmpl
  self.no_unset = no_unset
  self.sigil = sigil
  self.braces = braces
 
 def substitute(self, scope):
  return parser.Parser(self, scope).parse()
