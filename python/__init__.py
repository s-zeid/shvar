from . import errors
from .template import Template


def substitute(tmpl, scope=None, default=None,
               *, no_unset=False, sigil=None, braces=None):
 try:
  return Template(
   tmpl,
   no_unset=no_unset,
   sigil=sigil,
   braces=braces
  ).substitute(scope)
 except errors.SHVarError:
  if default is not None:
   return default
  else:
   raise
