from typing import *


class SHVarError(Exception):
 message: str
 
 def __init__(self, message: str):
  self.message = message
 
 def __str__(self):
  return self.message
 
 def __repr__(self):
  def arg_repr(value):
   return repr(value) if value != Ellipsis else "..."
  
  args = {"message": self.message}
  args.update(self._extra_args())
  args_repr = ", ".join(f"{name}={arg_repr(args[name])}" for name in args)
  return "%s(%s)" % (type(self).__name__, args_repr)
 
 def _extra_args(self) -> Dict[str, Any]:
  return {}


class OtherError(SHVarError):
 pass


class SyntaxError(SHVarError):
 pass


class KeyError(SHVarError):
 var_name: str
 maybe_null: bool
 
 def __init__(self, message: str, var_name: str, maybe_null: bool = True):
  super().__init__(message)
  self.var_name = var_name
  self.maybe_null = maybe_null
 
 def _extra_args(self) -> Dict[str, Any]:
  return dict(
   var_name=self.var_name,
   maybe_null=self.maybe_null,
  )
