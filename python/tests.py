import inspect
import sys

from . import errors
from .parser import Parser
from .template import Template


def get():
 return [
  [r"$x", {"x": "spam"}, r"spam", str, _line()],
  [r"${x}", {"x": "spam"}, r"spam", str, _line()],
  [r"${x:-egg}", None, r"egg", str, _line()],
  [r"${x-egg}", None, r"egg", str, _line()],
  [r"${x:=egg}", None, r"egg", str, _line()],
  [r"${x=egg}", None, r"egg", str, _line()],
  [r"${x:?error}", None, r"error", errors.KeyError, _line()],
  [r"${x?error}", None, r"error", errors.KeyError, _line()],
  [r"${x:+alternate}", None, r"", str, _line()],
  [r"${x+alternate}", None, r"", str, _line()],
  [r"${x:-egg}", {"x": ""}, r"egg", str, _line()],
  [r"${x-egg}", {"x": ""}, r"", str, _line()],
  [r"${x:=egg}", {"x": ""}, r"egg", str, _line()],
  [r"${x=egg}", {"x": ""}, r"", str, _line()],
  [r"${x:?error}", {"x": ""}, r"error", errors.KeyError, _line()],
  [r"${x?error}", {"x": ""}, r"", str, _line()],
  [r"${x:+alternate}", {"x": ""}, r"", str, _line()],
  [r"${x+alternate}", {"x": ""}, r"alternate", str, _line()],
  [r"asdf", None, r"asdf", str, _line()],
  [r"asdf $x", {"x": "spam"}, r"asdf spam", str, _line()],
  [r"asdf ${x}", {"x": "spam"}, r"asdf spam", str, _line()],
  [r"asdf ${x:-${y}}", {"y": "egg"}, r"asdf egg", str, _line()],
  [r": $x :", {"x": "spam"}, r": spam :", str, _line()],
  [r": ${x:-egg} :", {"x": ""}, r": egg :", str, _line()],
  [r": ${x:=egg} :", {"x": ""}, r": egg :", str, _line()],
  [r": ${x:-{$y\}}", {"y": "egg"}, r": {egg}", str, _line()],
  [r": ${y:-asdf} ${x} $x :", {"x": "spam"}, r": asdf spam spam :", str, _line()],
  [r": ${y:-as ${x} df} ${x} $x :", {"x": "spam"}, r": as spam df spam spam :", str, _line()],
  [r": ${y:-as ${x} df} ${x} $x :", {"y": "egg"}, r": egg   :", str, _line()],
  [r": ${y:-{${x}\}} ${x} $x :", {"x": "spam"}, r": {spam} spam spam :", str, _line()],
  [r"${a:-{${b:-${c:-{${d:-{\}}\}}\}}}", None, r"{{{}}}", str, _line()],
  [r": ${x} ${x:=2} ${x} :", None, r":  2 2 :", str, _line()],
  [r": ${x} ${x:=2} ${x} :", {"x": "spam"}, r": spam spam spam :", str, _line()],
  [r": ${x:?} :", None, r"", errors.KeyError, _line()],
  [r": ${x:?error} :", {"x": "spam"}, r": spam :", str, _line()],
  [r": ${x:?$z} :", {"z": "bacon"}, r"bacon", errors.KeyError, _line()],
  [r": ${x:+alternate} :", {"x": "spam"}, r": alternate :", str, _line()],
  [r": ${x:-${y}} :", {"y": "egg"}, r": egg :", str, _line()],
  [r": \asdf :", {"x": "spam"}, r": \asdf :", str, _line()],
  [r": \$x :", {"x": "spam"}, r": $x :", str, _line()],
  [r": \\$x :", {"x": "spam"}, r": \spam :", str, _line()],
  [r": \\\$x :", {"x": "spam"}, r": \$x :", str, _line()],
  [r": ${x:-\\$y} :", {"y": "egg"}, r": \egg :", str, _line()],
  [r": ${x:-\\\$y} :", {"y": "egg"}, r": \$y :", str, _line()],
  [r": $10 :", {"1": "baked beans"}, r": baked beans0 :", str, _line()],
  [r": ${10} :", {"10": "lobster thermidor"}, r": lobster thermidor :", str, _line()],
  [r": $^ :", None, r": $^ :", str, _line()],
  [r": ${^} :", None, None, errors.SyntaxError, _line()],
  [r": ${#x} :", {"x": "spam"}, r": 4 :", str, _line()],
  [r": ${#x:=2} $x :", None, None, errors.SyntaxError, _line()],
  [r": ${w#123} :", {"w": "123123abcabc"}, r": 123abcabc :", str, _line()],
  [r": ${w##123} :", {"w": "123123abcabc"}, r": 123abcabc :", str, _line()],
  [r": ${w%abc} :", {"w": "123123abcabc"}, r": 123123abc :", str, _line()],
  [r": ${w%%abc} :", {"w": "123123abcabc"}, r": 123123abc :", str, _line()],
  [r": ${w%%a${letter}c} :", {"w": "123123abcabc", "letter": "b"}, r": 123123abc :", str, _line()],
  [r": ${w%%${suffix}} :", {"w": "123123abcabc", "suffix": "abc"}, r": 123123abc :", str, _line()],
  [r": ${x#} :", {"x": "spam"}, r": spam :", str, _line()],
  [r": ${x##} :", {"x": "spam"}, r": spam :", str, _line()],
  [r": ${x%} :", {"x": "spam"}, r": spam :", str, _line()],
  [r": ${x%%} :", {"x": "spam"}, r": spam :", str, _line()],
  [r": ${x", None, None, errors.SyntaxError, _line()],
  [r": ${x:-{\}", None, None, errors.SyntaxError, _line()],
  [r": ${x#prefix", None, None, errors.SyntaxError, _line()],
  [r": ${x:-${y\}}", None, None, errors.SyntaxError, _line()],
  [r"${x:-'egg'}", None, r"'egg'", str, _line()],
  [r'${x:-"egg"}', None, r"egg", str, _line()],
  [r'${x:-\"egg\"}', None, r'"egg"', str, _line()],
  ['${y:-"b"a\'c\'o"n"}', None, r"ba'c'on", str, _line()],
  [r"${n:-'${asdf}'}", None, r"''", str, _line()],
  [r'${n:-"${asdf}"}', {"asdf": "spam"}, r"spam", str, _line()],
  [r'${n:-"${asdf"}}', None, None, errors.SyntaxError, _line()],
  [r'${n:-"${asdf:-"}}', None, None, errors.SyntaxError, _line()],
  [r'${n:-"${asdf:-"qwerty"}"}', None, r"qwerty", str, _line()],
  [r'${m:-"${n:-"${o:-"${p:-\"qwerty\"}"}"}"}', None, r'"qwerty"', str, _line()],
  ['${x:-"${y:-"${z:-"${a:-\'b\'}"}"}"}', None, r"'b'", str, _line()],
  [r'${a#"spam "}', {"a": "spam egg"}, "egg", str, _line()],
  ['${b#"b"a\'c\'o"n"}', {"b": "bacon/spam"}, "/spam", str, _line()],
  ['${b#"ba\'c\'on"}', {"b": "ba'c'on/spam"}, "/spam", str, _line()],
  [r"${b#ba'${y}c'on}", {"b": "ba${y}con/spam", "y": "egg"}, "/spam", str, _line()],
  [r"# ${x##\\} #", {"x": "\\spam"}, "# spam #", str, _line()],
  [r"# ${USERPROFILE##*\\} #", {"USERPROFILE": "S:\\Users\\Frog"}, "# Frog #", str, _line()],
  [r"# ${x##'*'} #", {'x': '*a'}, '# a #', str, _line()],
  [r'# ${x##"*"} #', {'x': '*a'}, '# a #', str, _line()],
  ['# ${x##"\'*\'"} #', {'x': "'*'a"}, '# a #', str, _line()],
  ['# ${x##"\'\\"*\\"\'"} #', {'x': "'\"*\"'a"}, '# a #', str, _line()],
  ['# ${x##"\'\\"*\'\\""} #', {'x': "'\"*'\"a"}, '# a #', str, _line()],
  ['# ${x##"\\"\'\\"*\\"\'\\""} #', {'x': "\"'\"*\"'\"a"}, '# a #', str, _line()],
  ['# ${x##"\'\\"\'\\"*\\"\'\\"\'"} #', {'x': "'\"'\"*\"'\"'a"}, '# a #', str, _line()],
  [r'# ${x#x1y} #', {'x': 'x1yzab'}, '# zab #', str, _line()],
  [r'# ${x#x?yza\}}cd} #', {'x': 'x1yza}'}, '# cd} #', str, _line()],
  [r'# ${x#x?yza\}cd} #', {'x': 'x1yza}cd'}, '#  #', str, _line()],
  [r'# ${x#x?yzab} #', {'x': 'x1yzab'}, '#  #', str, _line()],
  [r'# ${x#x?yzab} #', {'x': 'x12yzab'}, '# x12yzab #', str, _line()],
  [r'# ${x#x?yzab} #', {'x': 'x1yz'}, '# x1yz #', str, _line()],
  [r'# ${x#x?yzab} #', {'x': 'xyzab'}, '# xyzab #', str, _line()],
  [r'# ${x#x?yzab} #', {'x': 'x1'}, '# x1 #', str, _line()],
  [r'# ${x#x?} #', {'x': 'x1yzab'}, '# yzab #', str, _line()],
  [r'# ${x#x?yz} #', {'x': 'x1yzab'}, '# ab #', str, _line()],
  [r'# ${x#x?y*z*a} #', {'x': 'x1y23456za'}, '#  #', str, _line()],
  [r'# ${x#x?y*z*a} #', {'x': 'x1y23456na'}, '# x1y23456na #', str, _line()],
  [r'# ${x#x?y********************z*a} #', {'x': 'x1y23456za'}, '#  #', str, _line()],
  [r'# ${x#x?y***************?????z*a} #', {'x': 'x1y23456za'}, '#  #', str, _line()],
  [r'# ${x#x?y**************??????z*a} #', {'x': 'x1y23456za'}, '# x1y23456za #', str, _line()],
  [r'# ${x##x?y****??z*a} #', {'x': 'x1y23456za'}, '#  #', str, _line()],
  [r'# ${x##x?y****??z*a} #', {'x': 'x1y23456zab'}, '# b #', str, _line()],
  [r'# ${x#x?y****??z*a} #', {'x': 'x1y23456za'}, '#  #', str, _line()],
  [r'# ${x#x?y****????z*a} #', {'x': 'x1y23456za'}, '#  #', str, _line()],
  [r'# ${x#x?y****??????z*a} #', {'x': 'x1y23456za'}, '# x1y23456za #', str, _line()],
  [r'# ${x#x?y*[y-z]*a} #', {'x': 'x1y23456za'}, '#  #', str, _line()],
  [r'# ${x#x?y*z*a} #', {'x': 'x1y23456z'}, '# x1y23456z #', str, _line()],
  [r'# ${x#x?y*z} #', {'x': 'x1y23456za'}, '# a #', str, _line()],
  [r'# ${x#x\?y\*z\[0-9\]a} #', {'x': 'x?y*z[0-9]a'}, '#  #', str, _line()],
  [r'# ${x##[abc]} #', {'x': 'b'}, '#  #', str, _line()],
  [r'# ${x#[abc]} #', {'x': 'b'}, '#  #', str, _line()],
  [r'# ${x#[abc]} #', {'x': 'x'}, '# x #', str, _line()],
  [r'# ${x#[!abc]} #', {'x': 'b'}, '# b #', str, _line()],
  [r'# ${x#[!abc]} #', {'x': 'x'}, '#  #', str, _line()],
  [r'# ${x#[]abc]} #', {'x': ']'}, '#  #', str, _line()],
  [r'# ${x#[]abc]} #', {'x': 'a'}, '#  #', str, _line()],
  [r'# ${x#[!]bc]} #', {'x': ']'}, '# ] #', str, _line()],
  [r'# ${x#[!]bc]} #', {'x': 'b'}, '# b #', str, _line()],
  [r'# ${x#[a-zA-Z]} #', {'x': 'a'}, '#  #', str, _line()],
  [r'# ${x#[a-zA-Z]} #', {'x': 'B'}, '#  #', str, _line()],
  [r'# ${x#[a-zA-Z]} #', {'x': '0'}, '# 0 #', str, _line()],
  [r'# ${x#[!a-zA-Z]} #', {'x': 'c'}, '# c #', str, _line()],
  [r'# ${x#[!a-zA-Z]} #', {'x': '1'}, '#  #', str, _line()],
  [r'# ${x#[]} #', {'x': '[]'}, '#  #', str, _line()],
  [r'# ${x#[]} #', {'x': ']['}, '# ][ #', str, _line()],
  [r'# ${x#][} #', {'x': ']['}, '#  #', str, _line()],
  [r'# ${x#[} #', {'x': '['}, '#  #', str, _line()],
  [r'# ${x#[!]} #', {'x': '[!]'}, '#  #', str, _line()],
  [r'# ${x#[!]b} #', {'x': '[!]b'}, '#  #', str, _line()],
  [r'# ${x#[!]b} #', {'x': '[!]'}, '# [!] #', str, _line()],
  [r'# ${x#[!]b} #', {'x': '[!]b'}, '#  #', str, _line()],
  [r'# ${x##[!]b-c} #', {'x': '[!]a'}, '# [!]a #', str, _line()],
  [r'# ${x##[!]b-c} #', {'x': '[!]b'}, '# [!]b #', str, _line()],
  [r'# ${x##[!]b-c]} #', {'x': '[!]a'}, '# !]a #', str, _line()],
  [r'# ${x##[!]b-c]} #', {'x': '[!]b'}, '# !]b #', str, _line()],
  [r'# ${x##[?*} #', {'x': '[!c'}, '#  #', str, _line()],
  [r'# ${x#[!c} #', {'x': '[!c'}, '#  #', str, _line()],
  [r'# ${x#[!c} #', {'x': 'ab'}, '# ab #', str, _line()],
  [r'# ${x#[!!c]} #', {'x': '!b'}, '# !b #', str, _line()],
  [r'# ${x#[!!!]} #', {'x': 'ab'}, '# b #', str, _line()],
  [r'# ${x#[!!]} #', {'x': 'ab'}, '# b #', str, _line()],
  [r'# ${x#[!]} #', {'x': 'ab'}, '# ab #', str, _line()],
  [r'# ${x#[!!!]} #', {'x': '!b'}, '# !b #', str, _line()],
  [r'# ${x#[!!]} #', {'x': '!b'}, '# !b #', str, _line()],
  [r'# ${x#[!]} #', {'x': '!b'}, '# !b #', str, _line()],
  [r'# ${x#[!!!b]} #', {'x': 'ab'}, '# b #', str, _line()],
  [r'# ${x#[!!b]} #', {'x': 'ab'}, '# b #', str, _line()],
  [r'# ${x#[!b]} #', {'x': 'ab'}, '# b #', str, _line()],
  [r'# ${x#[!!!b]} #', {'x': '!b'}, '# !b #', str, _line()],
  [r'# ${x#[!!b]} #', {'x': '!b'}, '# !b #', str, _line()],
  [r'# ${x#[!b]} #', {'x': '!b'}, '# b #', str, _line()],
  [r'# ${x#[]123a-z456A-Z789-]} #', {'x': ']'}, '#  #', str, _line()],
  [r'# ${x#[]123a-z456A-Z789-]} #', {'x': '2'}, '#  #', str, _line()],
  [r'# ${x#[]123a-z456A-Z789-]} #', {'x': 'm'}, '#  #', str, _line()],
  [r'# ${x#[]123a-z456A-Z789-]} #', {'x': '6'}, '#  #', str, _line()],
  [r'# ${x#[]123a-z456A-Z789-]} #', {'x': 'M'}, '#  #', str, _line()],
  [r'# ${x#[]123a-z456A-Z789-]} #', {'x': '9'}, '#  #', str, _line()],
  [r'# ${x#[]123a-z456A-Z789-]*} #', {'x': '-?'}, '# ? #', str, _line()],
  [r'# ${x#[]123a-z456A-Z789-]*} #', {'x': '-]?'}, '# ]? #', str, _line()],
  [r'# ${x#[]123a-z456A-Z789-]*} #', {'x': '-]a'}, '# ]a #', str, _line()],
  [r'# ${x##[]123a-z456A-Z789-]*} #', {'x': '-?'}, '#  #', str, _line()],
  [r'# ${x##[]123a-z456A-Z789-]*} #', {'x': '-]?'}, '#  #', str, _line()],
  [r'# ${x##[]123a-z456A-Z789-]*} #', {'x': '-]a'}, '#  #', str, _line()],
  [r'% ${x%[abc]} %', {'x': 'b'}, '%  %', str, _line()],
  [r'% ${x%%[abc]} %', {'x': 'b'}, '%  %', str, _line()],
  [r'% ${x%x?y****??z*a} %', {'x': 'wx1y23456za'}, '% w %', str, _line()],
  [r'% ${x%%x?y****??z*a} %', {'x': 'wx1y23456za'}, '% w %', str, _line()],
  [r'% ${x%[]123a-z456A-Z789-]*} %', {'x': '?-'}, '% ? %', str, _line()],
  [r'% ${x%[]123a-z456A-Z789-]*} %', {'x': '?]-'}, '% ?] %', str, _line()],
  [r'% ${x%[]123a-z456A-Z789-]*} %', {'x': 'a]-'}, '% a] %', str, _line()],
  [r'% ${x%%[]123a-z456A-Z789-]*} %', {'x': '?-'}, '% ? %', str, _line()],
  [r'% ${x%%[]123a-z456A-Z789-]*} %', {'x': '?]-'}, '% ? %', str, _line()],
  [r'% ${x%%[]123a-z456A-Z789-]*} %', {'x': 'a]-'}, '%  %', str, _line()],
  [r'% ${x%*[]123a-z456A-Z789-]} %', {'x': '?-'}, '% ? %', str, _line()],
  [r'% ${x%*[]123a-z456A-Z789-]} %', {'x': '?]-'}, '% ?] %', str, _line()],
  [r'% ${x%*[]123a-z456A-Z789-]} %', {'x': 'a]-'}, '% a] %', str, _line()],
  [r'% ${x%%*[]123a-z456A-Z789-]} %', {'x': '?-'}, '%  %', str, _line()],
  [r'% ${x%%*[]123a-z456A-Z789-]} %', {'x': '?]-'}, '%  %', str, _line()],
  [r'% ${x%%*[]123a-z456A-Z789-]} %', {'x': 'a]-'}, '%  %', str, _line()],
 ]


def run(verbosity: int = 2, test_exec: str = None, newline_before_fail: bool = False):
 # Notes on `test_exec`
 # --------------------
 # 
 # `test_exec` is the path to (or name of on `$PATH`) a program that
 # purports to parse POSIX shell variable expansions like this one does.
 # In order for this test suite to work, that program MUST behave as
 # follows:
 # 
 # * It MUST accept a `--from-test` argument that causes it to handle
 #   errors and output as follows:
 #   
 #     * On syntax errors, the return code MUST be 3.  The error message
 #       SHOULD be printed to standard error and SHOULD NOT contain text
 #       like "program-name: error: ", but rather just the main message.
 #       (In this case, the error text is not significant to the test
 #       suite, but it will be formatted specially in the test suite's
 #       output.)
 #     
 #     * When a `?` or `:?` modifier causes an error, and an error message
 #       IS given on its right-hand side, the return code MUST be 5
 #       and that error message, and only that error message, MUST be
 #       printed on standard error.
 #     
 #     * When a `?` or `:?` modifier causes an error, and an error message
 #       IS NOT given on its right-hand side, the return code MUST be 4
 #       and text MUST NOT be printed on standard error (except for
 #       optionally one newline).
 #     
 #     * On other errors, the return code MUST be nonzero and MUST NOT be
 #       one of the return codes listed above.  In this case, standard
 #       error is not significant to the test suite but will be printed
 #       anyway and the test suite will then exit.
 #     
 #     * If the template is successfully evaluated, the result, and only
 #       the result, MUST be printed on standard output, and the return
 #       code MUST be zero.
 #     
 #     * In all cases, standard output and/or standard error MUST end
 #       with zero or one newlines (`\n`, `\r\n`, or `\r`), after any
 #       newlines that are part of the output or error message.
 # 
 # * It MUST accept zero or more `-v` arguments.  For each `-v` argument,
 #   the next argument will be of the form `NAME=VALUE`, and `VALUE` MUST
 #   be assigned to the name `NAME` in the template's substitution scope.
 #   When one or more `-v` arguments exist, the scope MUST NOT contain
 #   any variables not defined by a `-v` argument.
 # 
 # * The first argument that is not described above MUST be treated as
 #   the template string.  It MAY be joined with all subsequent arguments
 #   by a space character in order to form the final template string, but
 #   the test suite does not need this.
 # 
 # The test suite will not pass any arguments not described above.  It
 # also does not care how your program behaves when `--from-test` is not
 # in the argument list.
 
 if test_exec:
  verbosity += 1
 
 tests = get()
 
 pass_fmt = "\033[0;1;32m%s\033[0m" if sys.stdout.isatty() else "%s"
 fail_fmt = "\033[0;1;31m%s\033[0m" if sys.stdout.isatty() else "%s"
 
 n_total = len(tests)
 already_failed = False
 
 i = 1
 def run_test(tmpl, scope, want, want_type, line_no):
  nonlocal i
  nonlocal already_failed
  
  if not scope:
   scope = {}
  
  want_type = want_type.__name__
  
  if not test_exec:
   substitute = lambda tmpl, scope: Parser(Template(tmpl), scope).parse()
  else:
   def substitute(tmpl, scope):
    import subprocess
    
    def remove_newline(s):
     if s.endswith("\n"):
      s = s[:-1]
     if s.endswith("\r"):
      s = s[:-1]
     return s
    
    argv = [test_exec, "--from-test"]
    for k in scope:
     argv += ["-v", "%s=%s" % (k, str(scope[k]))]
    argv += [tmpl]
    
    run_result = subprocess.run(argv, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout = remove_newline(run_result.stdout.decode("utf-8"))
    stderr = remove_newline(run_result.stderr.decode("utf-8"))
    retcode = run_result.returncode
    
    if retcode:
     if retcode == 3:
      raise errors.SyntaxError(stderr)
     elif retcode == 4 or retcode == 5:
      raise errors.KeyError(stderr, ..., ...)
     else:
      print(stderr, file=sys.stderr)
      raise RuntimeError(f"`{test_exec}` failed with return code {retcode}")
    else:
     return stdout
  
  try:
   result = substitute(tmpl, scope)
   result_type = type(result).__name__
   result_display = f"`{result}`"
  except errors.SHVarError as exc:
   if not isinstance(exc, (errors.SyntaxError, errors.KeyError)):
    raise
   result = exc.message if isinstance(exc, errors.KeyError) else None
   result_type = type(exc).__name__
   result_display = repr(exc)
  except:
   raise
  
  passed = result == want and result_type == want_type
  
  status = "passed:" if passed else "FAILED:"
  term_fmt = "%s"
  if passed:
   status_fmt = pass_fmt
  else:
   term_fmt = status_fmt = fail_fmt
  
  if want_type == "KeyError":
   if passed and verbosity < 3:
    want_display = result_display
   else:
    want_display = repr(errors.KeyError(want, ..., ...))
  elif want_type != "str":
   if passed and verbosity < 3:
    want_display = result_display
   else:
    want_display = "%s(...)" % want_type
  else:
   want_display = f"`{want}`" if want is not None else "None"
  
  if verbosity >= 2 or (verbosity >= 1 and not passed):
   if not passed:
    if already_failed or newline_before_fail:
     print()
   i_display = f"%{len(str(n_total))}d/%d:" % (i, n_total)
   status_padding = " " * (len(i_display) + len(status) + 1)
   print(status_fmt % f"{i_display} {status}" +
         term_fmt % f" `{tmpl}`, {scope}  # line {line_no}")
   print(term_fmt % f"{status_padding} -> {want_display}")
  
  i += 1
  
  if verbosity >= 3 or (not passed and verbosity >= 1):
   got = "got:" if passed else "GOT:"
   got_padding = " " * (len(i_display) + max(len(status) - len(got), 0) + 1)
   print(term_fmt % f"{got_padding}{got} {result_display}")
  
  if not passed:
   already_failed = True
  
  return passed
 
 try:
  results = [run_test(*i) for i in tests]
 except RuntimeError as exc:
  if verbosity > 0:
   print()
  print(fail_fmt % exc, file=sys.stderr)
  return False
 
 ret = all(results)
 if verbosity > 0:
  if verbosity >= 2 or not ret:
   print()
  if ret:
   n = len([i for i in results if i])
   print(pass_fmt % f"{n}/{n_total} tests passed")
  else:
   n = len([i for i in results if not i])
   print(fail_fmt % f"{n}/{n_total} tests FAILED")
 
 return all(results)


def _line():
 return inspect.currentframe().f_back.f_lineno
