use std::collections::HashMap;

pub use self::pattern::Pattern;
pub use self::result::Error;
pub use self::result::ErrorCode;
pub use self::result::Result;
pub use self::template::Template;

pub mod parser;
pub mod pattern;
pub mod result;  // contains the definition of err!()
pub mod template;


pub fn substitute(
 tmpl: &str,
 scope: Option<&mut HashMap<String, String>>,
) -> result::Result<String> {
 return template::Template::new(tmpl).substitute(scope);
}


pub fn substitute_or(
 tmpl: &str,
 scope: Option<&mut HashMap<String, String>>,
 default: &str,
) -> String {
 return substitute(tmpl, scope).unwrap_or(default.to_string());
}
