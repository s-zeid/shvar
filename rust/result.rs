use std::error;
use std::fmt;


pub type Result<T> = std::result::Result<T, Error>;


pub enum ErrorCode {
 Other = 1,
 Syntax = 3,
 KeyNoMessage = 4,
 KeyWithMessage = 5,
}


#[derive(Debug)]
pub enum Error {
 Other { message: String },
 Syntax { message: String },
 Key { message: String, var_name: String, maybe_null: bool },
}

#[macro_export]
macro_rules! err {
 // err!(Variant, "message format literal"[, format_arg[, ...]]; key: value[, [...]])
 ($variant:ident, $message_format:expr$(, $($message_args:expr),*)?; $($key:ident: $value:expr),*) => {{
  Err($crate::result::Error::$variant {
   message: format!($message_format$(, $($message_args),*)?),
   $($key: $value),*
  })
 }};
 // err!(Variant, "message format literal"[, format_arg[, ...]])
 ($variant:ident, $message_format:expr$(, $($message_args:expr),*)?) => {{
  $crate::err!($variant, $message_format$(, $($message_args),*)?;)
 }};
}

impl Error {
 pub fn code(&self) -> ErrorCode {
  return match self {
   Error::Syntax { .. } => ErrorCode::Syntax,
   Error::Key { message, .. } if message.is_empty() => ErrorCode::KeyNoMessage,
   Error::Key { .. } => ErrorCode::KeyWithMessage,
   _ => ErrorCode::Other,
  };
 }

 pub fn message(&self) -> String {
  return match self {
   Error::Other { message } => message.clone(),
   Error::Syntax { message } => message.clone(),
   Error::Key { message, var_name, maybe_null } => {
    if !message.is_empty() {
     message.clone()
    } else if *maybe_null {
     var_name.clone() + ": parameter null or not set"
    } else {
     var_name.clone() + ": parameter not set"
    }
   },
  };
 }

 pub fn prefix(&self) -> String {
  return match self {
   Error::Other { .. } => "error: ",
   Error::Syntax { .. } => "syntax error: ",
   Error::Key { .. } => "",
  }.to_string();
 }
}

impl error::Error for Error {}

impl fmt::Display for Error {
 fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
  write!(f, "{}", self.message())
 }
}
