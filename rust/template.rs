use std::collections::HashMap;
use std::env;

use crate::result;
use crate::parser;


pub struct Template {
 pub(crate) tmpl: String,
 pub(crate) no_unset: bool,
 pub(crate) sigil: char,
 pub(crate) braces: String,
}


impl Template {
 pub fn new(tmpl: &str) -> Template {
  return Template::new_customized(tmpl, false, '\0', "{}");
 }

 pub fn new_customized(tmpl: &str, no_unset: bool, sigil: char, braces: &str) -> Template {
  return Template {
   tmpl: String::from(tmpl),
   no_unset: no_unset,
   sigil: if sigil != '\0' { sigil } else { '$' },
   braces: String::from(if !braces.is_empty() { braces } else { "{}" }),
  };
 }

 pub fn substitute(&self, scope: Option<&mut HashMap<String, String>>) -> result::Result<String> {
  let mut env_scope = env_to_map();
  let mut parser = match parser::Parser::new(&self, match scope {
   Some(map) => map,
   None => &mut env_scope,
  }) {
   Ok(parser) => parser,
   Err(error) => return Err(error),
  };
  return parser.parse();
 }
}


fn env_to_map() -> HashMap<String, String> {
 let mut result: HashMap<String, String> = HashMap::new();
 for (key, value) in env::vars() {
  result.insert(key, value);
 }
 return result;
}
