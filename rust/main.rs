extern crate shvar;

#[macro_use]
extern crate clap;

use std::collections::HashMap;
use std::env;
use std::process;


fn main() {
 let argv0 = match env::current_exe() {
  Ok(path) => path.file_name().unwrap().to_string_lossy().into_owned(),
  Err(_) => crate_name!().to_string()
 };

 let options = clap_app!(app =>
  (name: crate_name!())
  (version: crate_version!())
  (about: crate_description!())
  (@setting StrictUtf8)
  (@setting TrailingVarArg)
  (@arg hep: --hep +hidden)
  (@arg from_test: --("from-test") +hidden)
  (@arg vars: -v --var ... value_name("NAME=VALUE") number_of_values(1)
        "define a variable")
  (@arg program_name: -p --prog [PROGRAM] default_value(argv0.as_str())
        "the program name to use in null or unset error messages")
  (@arg TEMPLATE: ... required_unless[hep]
        "the template to evaluate")
 ).get_matches_safe().unwrap_or_else(|error| match error.kind {
  clap::ErrorKind::HelpDisplayed | clap::ErrorKind::VersionDisplayed => {
   println!("{}", error.message);
   process::exit(0);
  }, _ => {
   eprintln!("{}", error.message);
   process::exit(2);
  }
 });

 if options.is_present("hep") {
  println!("Hep!  Hep!  I'm covered in sawlder! ... Eh?  Nobody comes.");
  println!("--Red Green, https://www.youtube.com/watch?v=qVeQWtVzkAQ#t=6m27s");
  process::exit(0);
 }

 let from_test = options.is_present("from_test");

 let var_specs: Vec<&str> = match options.values_of("vars") {
  Some(values) => values.collect(),
  None => Vec::new(),
 };
 let mut user_scope: HashMap<String, String> = HashMap::new();
 for spec in var_specs {
  let parts: Vec<&str> = spec.splitn(2, "=").collect();
  if parts.len() != 2 {
   eprintln!("{}: error: invalid variable declaration: {}", argv0, spec);
   process::exit(2);
  }
  user_scope.insert(parts[0].to_string(), parts[1].to_string());
 }

 let tmpl_str: String = match options.values_of("TEMPLATE") {
  Some(values) => values.collect::<Vec<&str>>(),
  None => Vec::<&str>::new(),
 }.join(" ").to_string();

 let scope = if !user_scope.is_empty() { Some(&mut user_scope) } else { None };

 match shvar::substitute(&tmpl_str, scope) {
  Ok(result) => {
   println!("{}", result);
   process::exit(0);
  },
  Err(error) => {
   if !from_test {
    let program_name = options.value_of("program_name").unwrap();
    eprint!("{}: {}", program_name, error.prefix());
   }
   if !from_test || !matches!(error.code(), shvar::ErrorCode::KeyNoMessage) {
    eprintln!("{}", error.message());
   }
   process::exit(error.code() as i32);
  }
 };
}
