use std::collections::HashMap;

use crate::err;
use crate::result;
use crate::template::Template;
use crate::pattern::Pattern;


pub struct Parser<'p> {
 tmpl: Vec<char>,
 no_unset: bool,
 sigil: char,
 braces: Vec<char>,
 brackets: &'p str,

 scope: &'p mut HashMap<String, String>,
 pos: usize,
 brace_level: u64,
}


impl<'p> Parser<'p> {
 pub fn new(template: &Template, scope: &'p mut HashMap<String, String>) -> result::Result<Parser<'p>> {
  let mut parser = Parser {
   tmpl: template.tmpl.chars().collect(),
   no_unset: template.no_unset,
   sigil: template.sigil,
   braces: template.braces.chars().collect(),
   brackets: "[]",

   scope: scope,
   pos: 0,
   brace_level: 0,
  };

  if parser.braces.contains(&parser.sigil) {
   return err!(Other, "sigil `{}` and braces `{}` conflict", parser.sigil, template.braces);
  }

  if parser.is_important(parser.sigil, false, true) {
   return err!(Other, "`{}` is not a valid sigil", parser.sigil);
  }

  if parser.braces.len() != 2 || parser.braces[0] == parser.braces[1] {
   return err!(Other, "`{}` is not a valid set of braces", template.braces);
  }

  for brace in parser.braces.iter() {
   if parser.is_important(*brace, true, false) {
    return err!(Other, "`{}` is not a valid brace", brace);
   }
  }
  
  if parser.braces[0] == parser.brackets.chars().nth(0).unwrap() {
   parser.brackets = "{}";
  }

  return Ok(parser);
 }


 pub fn parse(&mut self) -> result::Result<String> {
  return self.parse_str(0, true, "");
 }


 fn parse_str(&mut self, start_brace_level: u64, ignore_single_quotes: bool, escape_quoted_chars: &str) -> result::Result<String> {
  let mut result = String::new();

  let mut in_double_quote = false;
  let mut in_single_quote = false;

  let escape_backslashes = escape_quoted_chars.contains('\\');

  while self.pos < self.tmpl.len() {
   let ch = self.tmpl[self.pos];

   if !in_single_quote {
    if ch == '\\' {
     if self.pos + 1 < self.tmpl.len() {
      let next_ch = self.tmpl[self.pos + 1];
      if ['\\', '\'', '"', self.sigil].contains(&next_ch) {
       if next_ch == '\\' && escape_backslashes {
        result.push('\\');
       }
       result.push(next_ch);
       self.pos += 2;
       continue;
      } else if self.brace_level > start_brace_level &&
                next_ch == self.braces[1] {
       result.push(next_ch);
       self.pos += 2;
       continue;
      }
     }
    }

    if ch == self.sigil {
     self.pos += 1;
     result.push_str(&self.parse_var()?);
     if self.pos >= self.tmpl.len() {
      break;
     }
     continue;
    }

    if !in_double_quote {
     if self.brace_level > start_brace_level && ch == self.braces[1] {
      self.brace_level -= 1;
      self.pos += 1;
      break;
     }
    }
   }

   if ch == '"' {
    in_double_quote = !in_double_quote;
   } else if ch == '\'' && !ignore_single_quotes && !in_double_quote {
    in_single_quote = !in_single_quote;
   } else {
    if in_single_quote || in_double_quote {
     if escape_quoted_chars.contains(ch) {
      result.push('\\');
     }
    }
    result.push(ch);
   }

   self.pos += 1;
   continue;
  }

  return Ok(result);
 }


 fn parse_var(&mut self) -> result::Result<String> {
  let start_brace_level = self.brace_level;
  let mut first_ch = true;
  let mut var_name = String::new();
  let mut want_length = false;
  let mut brace_op = '\0';
  let mut brace_op_double = false;
  let mut brace_op_rhs = String::new();
  let mut is_colon_op = false;
  let mut result = String::new();

  while self.pos < self.tmpl.len() {
   let mut ch = self.tmpl[self.pos];

   if self.brace_level > start_brace_level {
    if ch == self.braces[1] {
     self.brace_level -= 1;
     self.pos += 1;
     break;
    }
   }

   if first_ch {
    if ch == self.braces[0] {
     self.brace_level += 1;
     self.pos += 1;
     continue;
    }

    if self.is_ident_first(ch) {
     var_name.push(ch);
     first_ch = false;
    } else if self.is_number(ch) {
     var_name.push(ch);
     if self.brace_level <= start_brace_level {
      self.pos += 1;
      break;
     }
    } else if self.brace_level > start_brace_level {
     if ch == '#' {
      want_length = true;
      self.pos += 1;
      continue;
     } else if self.is_brace_special(ch) {
      return err!(Syntax, "no identifier found in brace expansion");
     } else {
      return err!(Syntax, "invalid character `{}` in brace expansion", ch);
     }
    } else {
     result.push(self.sigil);
     result.push(ch);
     self.pos += 1;
     break;
    }
   } else {  // not first_ch
    if self.is_ident(ch) {
     var_name.push(ch);
    } else if self.brace_level > start_brace_level {
     if ch == ':' {
      if self.pos + 1 < self.tmpl.len() {
       is_colon_op = true;
       self.pos += 1;
       ch = self.tmpl[self.pos];
      } else {
       break;
      }
     }
     if self.is_modifier(ch) {
      brace_op = ch;
      self.pos += 1;
      brace_op_rhs = self.parse_str(start_brace_level, true, "")?;
      break;
     } else if is_colon_op {
      let mut ch_string = ch.to_string();
      if ch == self.braces[1] {
       ch_string.clear();
      }
      return err!(Syntax, "invalid modifier `:{}` in brace expansion", ch_string);
     } else if ch == '#' || ch == '%' {
      brace_op = ch;
      self.pos += 1;
      if self.pos < self.tmpl.len() && self.tmpl[self.pos] == ch {
       brace_op_double = true;
       self.pos += 1;
      }
      brace_op_rhs = self.parse_str(start_brace_level, false, r"\?*[")?;
      break;
     } else {
      return err!(Syntax, "invalid modifier `{}` in brace expansion", ch)
     }
    } else {
     break;
    }
   }

   self.pos += 1;
   continue;
  }

  if self.brace_level > start_brace_level {
   return err!(Syntax, "unterminated brace expansion");
  } else if !var_name.is_empty() {
   let mut value = String::new();
   let mut val_is_set = false;
   match self.scope.get(&var_name) {
    Some(string) => {
     value = string.to_string();
     val_is_set = true;
    },
    None => {}
   }

   let use_value = if is_colon_op {
    !value.is_empty()
   } else {
    val_is_set
   };

   if !self.is_modifier(brace_op) && self.no_unset && !val_is_set {
    return err!(Key, ""; var_name: var_name, maybe_null: false);
   }

   if want_length {
    if brace_op != '\0' {
     return err!(Syntax, "cannot use length operator and modifier at the same time");
    }
    result = value.len().to_string();
   } else if brace_op != '\0' {
    match brace_op {
     '-' => {
      result = if use_value { value } else { brace_op_rhs };
     }
     '=' => {
      if !use_value {
       result = brace_op_rhs;
       self.scope.insert(var_name, result.clone());
      } else {
       result = value;
      }
     }
     '?' => {
      if use_value {
       result = value;
      } else {
       return err!(Key, "{}", brace_op_rhs; var_name: var_name, maybe_null: is_colon_op);
      }
     }
     '+' => {
      result = if use_value { brace_op_rhs } else { String::new() };
     }
     '#' => {
      result = value;
      if !brace_op_rhs.is_empty() {
       let pat = Pattern::with_brackets(&brace_op_rhs, self.brackets)?;
       result = pat.remove_from_start(&result, brace_op_double).to_string();
      }
     }
     '%' => {
      result = value;
      if !brace_op_rhs.is_empty() {
       let pat = Pattern::with_brackets(&brace_op_rhs, self.brackets)?;
       result = pat.remove_from_end(&result, brace_op_double).to_string();
      }
     }
     _ => {}
    }
   } else {
    result = value;
   }
  }

  return Ok(result);
 }


 pub fn is_letter(&self, ch: char) -> bool {
  return matches!(ch, 'A'..='Z') || matches!(ch, 'a'..='z');
 }

 pub fn is_number(&self, ch: char) -> bool {
  return matches!(ch, '0'..='9');
 }

 pub fn is_ident_first(&self, ch: char) -> bool {
  return self.is_letter(ch) || ch == '_';
 }

 pub fn is_ident(&self, ch: char) -> bool {
  return self.is_ident_first(ch) || self.is_number(ch);
 }

 pub fn is_modifier(&self, ch: char) -> bool {
  return "-=?+".contains(ch);
 }

 pub fn is_brace_special(&self, ch: char) -> bool {
  return self.is_modifier(ch) || ":#%".contains(ch);
 }

 pub fn is_important(&self, ch: char, is_brace: bool, is_sigil: bool) -> bool {
  if self.is_ident(ch) || ch == '\\' {
   return true;
  }
  if self.is_brace_special(ch) && !is_sigil {
   return true;
  }
  if ch == self.sigil && !is_sigil {
   return true;
  }
  if self.braces.contains(&ch) && !is_brace {
   return true;
  }
  return false;
 }
}
