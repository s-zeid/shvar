use crate::err;
use crate::result;


enum PatternItem {
 One,
 Any,
 Ranges(PatternRanges),
 String(Vec<char>),
}


struct PatternRange {
 start: char,
 end: char,
}

impl PatternRange {
 fn matches(&self, ch: char) -> bool {
  return self.start <= ch && ch <= self.end;
 }
}


struct PatternRanges {
 ranges: Vec<PatternRange>,
 inverse: bool,
}

impl PatternRanges {
 fn contain(&self, ch: char) -> bool {
  let mut test_ch_matches = self.ranges.iter().any(|range| range.matches(ch));
  if self.inverse {
   test_ch_matches = !test_ch_matches;
  }
  return test_ch_matches;
 }
}


pub struct Pattern {
 chars: Vec<char>,
 items: Vec<PatternItem>,
 brackets: Vec<char>,
 is_literal: bool,
}

impl Pattern {
 pub fn new(pattern: &str) -> result::Result<Pattern> {
  return Self::with_brackets(pattern, "[]");
 }

 pub fn with_brackets(pattern: &str, brackets: &str) -> result::Result<Pattern> {
  let mut pat = Pattern {
   chars: pattern.chars().collect(),
   items: Vec::new(),
   brackets: brackets.chars().collect(),
   is_literal: true,
  };

  if pat.brackets.len() != 2 || pat.brackets[0] == pat.brackets[1] {
   return err!(Other, "`{}` is not a valid set of brackets", brackets);
  }

  for bracket in pat.brackets.iter() {
   if *bracket == '?' || *bracket == '*' {
    return err!(Other, "`{}` is not a valid bracket", bracket);
   }
  }

  pat.compile()?;

  return Ok(pat);
 }

 fn compile(&mut self) -> result::Result<()> {
  if !self.items.is_empty() {
   return err!(Other, "already compiled");
  }

  let mut p_pos = 0usize;

  self.is_literal = true;

  while p_pos < self.chars.len() {
   let mut prev_item = self.items.last();
   let mut ch = self.chars[p_pos];
   p_pos += 1;

   match ch {
    '\\' => {
     ch = self.chars[p_pos];
     p_pos += 1;
    },
    '?' => {
     self.is_literal = false;
     self.items.push(PatternItem::One);
     continue;
    }, 
    '*' => {
     self.is_literal = false;
     if self.items.is_empty() || !matches!(prev_item, Some(PatternItem::Any)) {
      self.items.push(PatternItem::Any);
     }
     continue;
    },
    _ if ch == self.brackets[0] => {
     self.is_literal = false;
     let mut inverse = false;
     let mut ranges: Vec<PatternRange> = Vec::new();
     let mut bracket_pos = 0usize;
     let mut first_range = true;
     loop {
      if p_pos >= self.chars.len() {
       p_pos -= bracket_pos;
       bracket_pos = 0;
       ch = self.brackets[0];
       break;
      }
      ch = self.chars[p_pos];
      if !first_range && ch == self.brackets[1] {
       p_pos += 1;
       break;
      }
      if bracket_pos == 0 && ch == '!' {
       inverse = true;
       bracket_pos += 1;
      } else if p_pos + 2 < self.chars.len() && self.chars[p_pos + 1] == '-' &&
                self.chars[p_pos + 2] != self.brackets[1] {
       let start = ch;
       let end = self.chars[p_pos + 2];
       if start > end {
        return err!(Syntax, "invalid character range `{}`", &self.chars[p_pos..p_pos+2].iter().collect::<String>());
       }
       ranges.push(PatternRange { start: start, end: end });
       first_range = false;
       bracket_pos += 3;
       p_pos += 2;
      } else {
       ranges.push(PatternRange { start: ch, end: ch });
       first_range = false;
       bracket_pos += 1;
      }
      p_pos += 1;
     }
     if bracket_pos > 0 {
      self.items.push(PatternItem::Ranges(PatternRanges { ranges: ranges, inverse: inverse }));
      continue;
     }
    },
    _ => (),
   }

   prev_item = self.items.last();
   if let Some(PatternItem::String(_)) = prev_item {
    if let PatternItem::String(mut chars) = self.items.pop().unwrap() {
     chars.push(ch);
     self.items.push(PatternItem::String(chars));
    } else {
     unreachable!();
    }
   } else {
    self.items.push(PatternItem::String(vec!(ch)));
   }
  }

  return Ok(());
 }

 pub fn matches(&self, test: &str) -> bool {
  return self.matches_from(&test.chars().collect::<Vec<char>>(), 0);
 }

 fn matches_from(&self, test: &[char], from: usize) -> bool {
  let mut result = true;

  let mut p_pos = from;
  let mut t_pos = 0usize;

  while p_pos < self.items.len() {
   let item = &self.items[p_pos];

   match item {
    PatternItem::One => {
     if t_pos < test.len() {
      t_pos += 1;
     } else {
      result = false;
     }
    },
    PatternItem::Any => {
     let next_item = self.items.get(p_pos + 1);
     while t_pos < test.len() {
      match next_item {
       Some(PatternItem::String(chars)) if test.get(t_pos) != chars.get(0) => {
        t_pos += 1;
       },
       None => {
        t_pos += 1;
       },
       _ => {
        break;
       },
      }
     }
     if !matches!(next_item, None) {
      while t_pos < test.len() {
       if self.matches_from(&test[t_pos..], p_pos + 1) {
        return true;
       }
       t_pos += 1;
      }
      result = false;
     }
    },
    PatternItem::Ranges(ranges) => {
     if t_pos < test.len() && ranges.contain(*test.get(t_pos).unwrap()) {
      t_pos += 1;
     } else {
      result = false;
     }
    },
    PatternItem::String(chars) => {
     if t_pos < test.len() && test[t_pos..].starts_with(&chars) {
      t_pos += chars.len();
     } else {
      result = false;
     }
    },
   }

   if !result {
    break;
   }

   p_pos += 1;
  }

  if p_pos < self.items.len() || t_pos < test.len() {
   result = false;
  }

  return result;
 }

 pub fn remove_from_start<'v>(&self, value: &'v str, largest: bool) -> &'v str {
  let mut prefix_end_range: Box<dyn DoubleEndedIterator<Item = usize>>;
  prefix_end_range = Box::new(1..=value.len());
  if largest {
   prefix_end_range = Box::new(prefix_end_range.rev());
  }

  for prefix_end in prefix_end_range {
   if value.is_char_boundary(prefix_end) {
    let prefix_candidate = &value[0..prefix_end];
    if self.matches(prefix_candidate) {
     return &value[prefix_end..];
    }
   }
  }

  return value;
 }

 pub fn remove_from_end<'v>(&self, value: &'v str, largest: bool) -> &'v str {
  let mut suffix_start_range: Box<dyn DoubleEndedIterator<Item = usize>>;
  suffix_start_range = Box::new(0..=value.len()-1);
  if !largest {
   suffix_start_range = Box::new(suffix_start_range.rev());
  }

  for suffix_start in suffix_start_range {
   if value.is_char_boundary(suffix_start) {
    let suffix_candidate = &value[suffix_start..];
    if self.matches(suffix_candidate) {
     return &value[0..suffix_start];
    }
   }
  }

  return value;
 }
}
