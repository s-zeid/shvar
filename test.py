#!/usr/bin/env python3

import os
import subprocess
import sys


REF_LANGUAGE = "python"
ROOT = os.path.dirname(__file__) or "."


def call(language, *args, **kwargs):
 dir_ = os.path.join(ROOT, language)
 return subprocess.call(*args, cwd=dir_, **kwargs)


def build(language, *, show_errors=False):
 errors = None if show_errors else subprocess.DEVNULL
 return call(language, ["make"], stdout=subprocess.DEVNULL, stderr=errors)


def test(language):
 nlbf = "--test-newline-before-fail"
 if language == REF_LANGUAGE:
  return call(language, ["./shvar", "--test", nlbf])
 else:
  return call(language, [f"../{REF_LANGUAGE}/shvar", "--test-exec", "./shvar", nlbf])


def main(argv):
 r = build("python", show_errors=True)
 if r != 0:
  return r

 args_languages = [i for i in argv[1:] if not i.startswith("-")]

 test_languages = []
 prod_languages = []
 for i in os.listdir(ROOT):
  if os.path.isdir(os.path.join(ROOT, i)) and not i.startswith("."):
   if os.path.isfile(os.path.join(ROOT, i, "GNUmakefile")):
    if i.endswith(".test"):
     test_languages += [i]
    else:
     prod_languages += [i]
 test_languages.sort()
 prod_languages.sort()

 languages = test_languages[:]
 if args_languages:
  for language in args_languages:
   if language in test_languages:
    continue
   if language in prod_languages:
    languages += [language]
   else:
    print(f"error: language `{language}` has no implementation", file=sys.stderr)
    return 1
 else:
  languages += prod_languages

 all_passed = True
 for language in languages:
  print(f"{language}: ", end="")
  sys.stdout.flush()
  r = build(language)
  if r != 0:
   all_passed = False
   print(f"make failed")
   continue
  r = test(language)
  if r != 0:
   all_passed = False
 
 return 0 if all_passed else 1


if __name__ == "__main__":
 try:
  sys.exit(main(sys.argv))
 except KeyboardInterrupt:
  pass
